
Step by step to run the project in localhost using docker-compose


Clone the project. The project is on git in private account.
git clone https://gitlab.com/vini.freire.oliveira/incredible_graphics.git


Build the containers and install all dependencies
docker-compose run --rm app bundle install


Now, create database and generate migrations:
docker-compose run --rm app bundle exec rails db:create db:migrate db:seed


Start the server with
docker-compose up


Open browser in Localhost:3000.


Links

My Git


My LikedIn