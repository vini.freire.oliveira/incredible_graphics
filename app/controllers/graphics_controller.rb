class GraphicsController < ApplicationController
  before_action :set_graphics, only: [:index]

  def index
  end

  private

  def set_graphics
    @male_user = User.where(gender: :male).group_by_week(:birthday).count
    @female_user = User.where(gender: :female).group_by_week(:birthday).count
  end
end
